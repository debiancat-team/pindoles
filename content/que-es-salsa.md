# Què és Salsa?

## Introducció
[Salsa][salsa] és una de les eines de Debian *gairebé* indispensable per
col·laborar a Debian. Com sabem hi ha moltes maneres de col·laborar-hi, però
moltes d'elles passen per a tenir-hi certa interacció, de manera que és bo saber
per a què serveix realment i com treure'n partit.

[Salsa][salsa], tal com descriu la [wiki][wiki], és un sistema de
desenvolupament col·laboratiu de Debian. Dit així et pot deixar indiferent, però
el cas és que és una eina fonamental perquè centralitza els repositoris de codi
que defineixen els paquets de Debian.

Si hi accedeixes, veuràs que es tracta d'una instància del programari lliure
[Gitlab][gitlab] allotjada a Debian, i que té diversos grups separats per
temàtica. Però no només això.

### Història
El 2003 es van voler unificar tots els diferents repositoris que formen part de
Debian sota un mateix lloc, i aquest es va anomenar *Alioth*. Era un gestor de
continguts (concretament [FusionForge][fusionforge]) que tenia tant repositoris
com una secció de notícies, així com també permetia una certa interacció entre
usuaris. Amb el pas del temps també s'hi van afegir
[llistes de correu][alioth-lists].

Per determinats motius es va decidir que el sistema tenia una sèrie de mancances
que van fer necessari buscar-ne un substitut, i el 2018 es va anunciar que el
seu successor, [Salsa][salsa] ja estava llest.

## Avui en dia
Avui en dia Salsa és un conjunt de repositoris Git que emmagatzema el codi que
conformarà els paquets que els usuaris de Debian tenen disponibles.

Però això seria una mica incomplet, perquè també hi ha diferents tipus de
col·laboració com el [codi font de les traduccions de la pàgina web][debian-www]
o [aquest document que estàs llegint][pindoles]. Ja ho veus, no tot és
estrictament tècnic.

A més, com a sistema de repositoris Git actual, també ve amb eines associades
per simplificar el procés de generació de versions de programari, que és el que
es coneix com a Integració Contínua o CI/CD (resumint-t'ho es tracta d'unes
tasques que es poden automatitzar i executar-se en determinades condicions com
per exemple revisar que no hi hagi errors de sintaxi abans de publicar nous
canvis a la branca principal).

I no només això, sinó que també té repositoris privats, de manera que si vols
aprendre com funciona Git o vols compartir trossos de codi o estàs treballant
en algun paquet, pots allotjar-hi el teu codi.

Però a més a més, Salsa també actua com a sistema **SSO** (Single Sign-on), de
manera que el fet de tenir-hi un compte et permetrà usar diferents eines de
Debian amb les mateixes credencials (i si ja t'has identificat, no caldrà que ho
facis de nou).

Per tant, si vols continuar col·laborant amb Debian, tard o d'hora és possible
que necessitis crear-t'hi un usuari, així que t'animo a fer-ho.

## Som-hi
Crear un usuari a Salsa és molt senzill, ja que és el típic procés de registre
que et pots trobar en multitud de serveis web:

1. Vés a https://salsa.debian.org/users/sign_up
2. Pots triar si crear un usuari proporcionant el teu nom, una adreça de correu
(on hi rebràs les notificacions) i un usuari i contrasenya. Fins fa uns mesos,
els usuaris havien d'acabar amb `-guest` però ara ja no cal.
3. Confirma'n la creació clicant a l'enllaç que hauràs d'haver rebut al correu.

Si en el pas `2.` has decidit identificar-te usant un altre proveïdor, només
hauràs d'autoritzar l'accés.

Un cop t'hagis donat d'alta podràs reforçar la seguretat del compte.

### MFA (Multi Factor Authentication)
Per dificultar que algú et pugui suplantar fins i tot coneixent la teva
contrasenya, pots activar aquesta opció que demanarà que tinguis una aplicació
compatible (típicament en un mòbil) que generi codis d'un sol ús periòdicament.
D'aquesta manera, cada cop que iniciïs sessió se't demanarà que a més de la
contrasenya escriguis el codi que l'aplicació del mòbil hagi generat.

Els passos per activar el MFA són:
1. Ves a la configuració del teu usuari: https://salsa.debian.org/-/profile i activa l'edició (clica damunt la icona del llapis a la part superior dreta de la pàgina).
2. Vés a la secció *Account*: https://salsa.debian.org/-/profile/account.
3. Activa l'opció **Two-Factor Authentication**.
4. Assegura't que tens una aplicació compatible com per exemple [Aegis][aegis].
5. Escaneja el codi QR per assegurar que el compte es vinculi amb l'aplicació.
6. Verifica amb codis que l'aplicació generi que l'autenticació funciona.
7. Desa els 10 codis de recuperació per si de cas et cal entrar però no
disposes del mòbil.

### Claus SSH
Aquest punt és opcional si tens intenció de treballar amb Git.

Si afegeixes la clau pública SSH del teu ordinador (més exactament la clau
de l'usuari amb el que inicies la sessió), podràs interactuar amb Git a través
de SSH en comptes de HTTPS, de manera que cada cop que facis un `git push`
t'estaràs autenticant de forma transparent i és més còmode.

Els passos per pujar la clau SSH són:
1. Ves a la configuració del teu usuari: https://salsa.debian.org/-/profile
2. Ves a la secció *SSH Keys*: https://salsa.debian.org/-/profile/keys
3. Obre una finestra de línia de comandes (terminal) i escriu
`cat ~/.ssh/id_rsa.pub`: això mostra només la clau pública del parell de claus
SSH, de manera que és segur copiar-la. En el cas que no en tinguis cap, n'hauràs
de crear una amb la comanda `ssh-keygen`.
4. Copia i enganxa el resultat del punt anterior i prem el botó *Add Key*.
Hauria de tenir un format similar al següent:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLbWePqSAy6mKGELx5+KTVbaxtt6Oe4Xt9ALqOsyUVvpEqqeFZUYAH7zJr1bTLVdjMIlWAWAXZWx6UoPGYpftsqutRczkWxKG6vt1qDNy5dlNp2Mp+/sKzQOC02yUKukzwDpWBC4oNp/rxQKkta7KoyNhvSGwa4p7Jb8pTy6KhrJDIqWlktx9Od7ieQsQHJO+/9RwtvZ0frvrX01TRbJbipFuvGXi6Ibz11cs0RdRpLeqMQk/0xUNIHYASffI0e5kxhS6f4EPhETuhrMppyxb8Pc2xc8ZMfB0b4nV+uN6/XW2XQnCswArOsmSN1CPVlRVsuf/UsjehLDubCdixo1tx6MWPbZqNyJjUkfr9dzCeIw7vROU2oRkn8eSnciwzlX1tUJqcjPUEluIOPeBsrsLG1bO2mfKlWuBxC1UCeDoHOgw3/3OIwIoseMkNr0wjssbNuoPl2q4C5lRg8+PDdSPOVat+HLf++9UMsCuVVmHcyFpRmxY+II0g+LT10uvUWPU= vagrant@debian
```

### Claus GPG
Aquest punt és opcional si tens intenció de treballar amb Git.

L'única manera d'assegurar que el contingut que es distribueix a la xarxa està
creat per l'autor legítim i que no s'ha alterat és signar-lo digitalment.
Aquest procés s'acostuma a fer amb GPG, i tot i que potser el cas d'ús és el
d'encriptar correus, GPG també permet la signatura digital, no només de correu
sinó també de documents… i de `commits` de Git.

És per tant una bona pràctica per assegurar que les modificacions al codi estan
signades, i es pot assegurar la integritat de l'autoria en tot moment.

Els passos per pujar la clau GPG (Pública) són:
1. Ves a la configuració del teu usuari: https://salsa.debian.org/-/profile
2. Ves a la secció *GPG Keys*: https://salsa.debian.org/-/profile/gpg_keys
3. Obre una finestra de línia de comandes (terminal) i escriu
`gpg --export --armor email@example.com`, on `email@example.com` és l'adreça
associada amb la teva clau. En cas que no en tinguis cap, n'hauràs de crear una
(de com a mínim 4096 bits!) amb la comanda `gpg --gen-key`.
4. Copia i enganxa el resultat del punt anterior i prem el botó *Add key*.
Hauria de tenir un format similar al següent:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBFEb40gBEAC2QX3rvtOPc+pKOMpLj565qmOE+48JAUEzUvW46xVdCoL8Huxj
vLzddtikqOTqKAujRLEUbdtb9OHm4FKDkZybYCpPPMF9/QJo1mzrDFjO3QyVfeHK
…
437URmUBALPob0SFMOqzvQ7G8i2lzLqz9jbm/70QhuARRYQ4Od8xAQD3eimp9sVs
2NTz4LU9JJPeioJK4vKG3BpzN0fb9tuKWQ==
=S4b6
-----END PGP PUBLIC KEY BLOCK-----
```

## Resultat
**Salsa** és una de les eines claus a Debian principalment perquè engloba els
repositoris de codi, però també és molt útil perquè permet un sistema
d'autenticació únic en d'altres aplicacions de tot l'ecosistema de Debian.

Tenir-hi usuari és simple i útil, de manera que si encara no en tens, és senzill
i recomanable crear-te'n un, especialment si vols col·laborar a Debian.

El grup local [DebianCat][debiancat] també hi tenim un repositori, actualment
només amb [píndoles][pindoles] com aquesta que estàs llegint. Així que si vols
crear un article com aquest, per què no t'hi afegeixes?

[salsa]: https://salsa.debian.org
[wiki]: https://wiki.debian.org/Salsa
[gitlab]: https://about.gitlab.com
[fusionforge]: https://fusionforge.org
[alioth-lists]: https://alioth-lists.debian.net/cgi-bin/mailman/listinfo
[debian-www]: https://salsa.debian.org/webmaster-team/webwml
[pindoles]: https://salsa.debian.org/debiancat-team/pindoles
[aegis]: https://getaegis.app
[debiancat]: https://salsa.debian.org/debiancat-team
