# Què és Debian Academy?

> Actualització: [A partir del 30/09/2021 el proveïdor del servei ha aturat el servidor degut a la inactivitat.][sundown]

## Introducció
Un altre projecte que està arrencant aquests últims mesos és
[Debian Academy][academy], una plataforma que té intenció d'ajudar amb la
documentació al voltant de Debian.

Tenint en compte que la forma més típica i visible tradicionalment (però no 
única ni més important que d'altres) de col·laboració a Debian és a través de
l'empaquetament, és a dir, el compromís de preparar nous programes (i mantenir
els actuals) perquè estiguin disponibles a l'arxiu de Debian en totes les
arquitectures suportades i amb un nivell d'exigència determinat.

El procés en sí no ha variat molt al llarg dels anys però com que depenent del
programa a empaquetar el procés pot variar notablement, amb el pas del temps
s'han anat creant diferents guies i documentacions que, per embolicar més les
coses, poden presentar diverses formes per arribar al mateix objectiu.

Sent conscients de la situació i d'adaptar la documentació a plataformes més
actuals (documentació se'n pot trobar en [fitxers de text pla][wouter-dgit] en
[vídeos][d-social-pony] a la [wiki][wiki-packaging] o la [pàgina oficial][dnmg]
o de forma efímera a diversos canals d'IRC, entre d'altres), s'ha iniciat
el projecte amb el recolzament de [Teckids][teckids].

## Som-hi
Ara mateix es tracta d'un projecte molt jove, així que no hi ha gaires cursos
disponibles, així que no podrem *consumir* massa informació.
A causa d'això, l'ajuda que hi podrem proporcionar es basa al voltant de la
creació de documentació (tant interna com de cursos).

El millor que pots fer és [accedir-hi][academy-moodle] per comprovar-ho en
primera persona. Es tracta d'una instància de [Moodle][moodle], o sigui que si
tens coneixements d'aquesta plataforma o qualsevol de les tecnologies que usa,
pots oferir-la també.
Tot i que la idea és que els cursos siguina accessibles sense necessitat de
registre, si t'hi vols identificar (per exemple, per fer un seguiment dels
cursos que t'interessin) pots usar [el teu usuari de Salsa][salsa].

A la [wiki][academy] s'expliquen les diverses formes de col·laborar-hi.

## Contacte
Com ja ve sent habitual, les formes de contacte tradicionals són el canal d'IRC
`#debian-academy` (OFTC) (pots consultar
[com interactuar amb IRC des de Matrix][matrix]) i una [llista de correu][ml].

## Resultat
Com tot projecte nou i dut a terme per voluntaris, necessita de tant més
col·laboració com continguts, així que en podem esperar resultats a mig termini.
És un projecte prometedor en el sentit que s'està revisant el que hi ha ara
mateix, s'hi està intentant posar ordre i, amb la plataforma d'aprenantatge
juntament amb els [vídeos de Peertube de Debian Social][d-social-peertube]
s'espera que la documentació pugui ser consumida de forma més amigable i per
tant més agradable per qui s'animi a col·laborar a Debian.

[sundown]: https://lists.debian.org/debian-academy/2021/09/msg00000.html
[academy]: https://wiki.debian.org/DebianAcademy
[wouter-dgit]: http://mdcc.cx/debian/debian-packaging-git.txt
[d-social-pony]: https://peertube.debian.social/videos/watch/27f7e9f3-77e7-4e6c-92a3-7ce84ea034f4
[wiki-packaging]: https://wiki.debian.org/Packaging
[dnmg]: https://www.debian.org/doc/manuals/maint-guide/index.html
[teckids]: https://www.teckids.org/en/
[academy-moodle]: https://academy.debian.net
[moodle]: https://moodle.org/
[salsa]: que-es-salsa.md
[matrix]: com-interactuar-amb-irc-des-de-matrix.md
[ml]: https://lists.debian.org/debian-academy/
[d-social-peertube]: https://peertube.debian.social/
