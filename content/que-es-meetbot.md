# MeetBot, una eina per gestionar reunions

## Introducció

Al ser una comunitat internacional, transversal i sobretot amb una
llarga trajectòria, Debian s'ha gestionat típicament amb les eines que
hi havia disponibles en el seu moment, sense evolucionar massa en el
sentit que si una cosa funciona, no cal canviar-la.

Això és especialment rellevant en el cas de la comunicació, que
majoritàriament es fa a través de [llistes de distribució][ml] o
sobretot en cas de ser en temps real, a través de canals d'IRC (als
que ens hi podem afegir amb un client o amb [eines més recents com Matrix][pindola-irc-matrix]).

En el cas de les reunions, especialment en aquelles que s'hi ha de
prendre decisions, s'acostuma a usar una eina que s'anomena [MeetBot][mb].

## Per a què serveix el Meetbot?

Per explicar breument què fa el [MeetBot][mb], només cal dir que
_escolta_ la reunió i fa una acta amb el que s'ha acordat o decidit,
que també inclourà accions que s'hagin de dur a terme. A més, tant
l'acta com el contingut de la reunió quedaran publicats a una pàgina
perquè es puguin consultar més endavant.

## Posa MeetBot a la teva vida

Només cal convidar-lo al canal d'IRC que vulguis (o afegir-lo, ja que
tècnicament no té criteri per decidir si vol acceptar o rebutjar la
invitació) i qualsevol membre del canal el podrà començar a usar.

No, no cal instal·lar res al teu ordinador perquè és un servei
permanent de l'IRC de Debian (`irc.debian.org`). Fàcil, oi?

## I com funciona?

Bàsicament amb les ordres que pots trobar documentades a la seva
[secció de la Wiki][mb]: tot i que inicialment farà cas a qui
l'invoqui en començar una reunió (el que es coneix com a _chair_, que
no deixa de un rol de secretari i pot canviar de mans en qualsevol
moment de la reunió) usant l'ordre `#startmeeting`, tothom el pot usar
en qualsevol moment per exemple per posar recordatoris (si s'escriu al
canal `MeetBot: pingall meeting in 43 minutes` esmentarà a cada usuari
amb el missatge `meeting in 43 minutes`).

A mesura que la reunió avanci, informarà del tema que s'està tractant
i registrarà els acords o accions que es vagin decidint. Aquí en tens
[un exemple pràctic][exemple] de quan es va usar per organitzar la
[MiniDebConf que es va celebrar a Barcelona l'any 2014][bcn2014].

Com veuràs, l'acta està en anglès! Això només és així perquè en aquell
cas concret no tothom parlava català: mentre al MeetBot li escriguis
les ordres en anglès, **la reunió es pot fer en la llengua que
prefereixis**.

I per què hi ha enllaçada una reunió tan antiga (2014)? Doncs perquè és
la darrera vegada que s'ha usat per la comunitat de parla catalana.

Potser, amb l'excusa d'aprendre a fer-lo servir, seria un bon moment per
reviure la comunitat i de pas sentir-se còmode usant una eina que usa
gran part de la Comunitat.

[ml]: https://lists.debian.org
[pindola-irc-matrix]: com-interactuar-amb-irc-des-de-matrix.md
[mb]: https://wiki.debian.org/MeetBot
[exemple]: http://meetbot.debian.net/debian-bcn2014/2014/debian-bcn2014.2014-03-09-10.33.html
[bcn2014]: https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014
