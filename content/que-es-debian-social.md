# Què és Debian Social?

## Introducció
Un grup de Debian Developers va voler crear una plataforma de diferents serveis
en la qual tot col·laborador hi fos benvingut independentment de la seva
vinculació amb Debian, i que servís per donar a conèixer què fan i fomentar la
col·laboració amb d'altres.

Tot i que ja existeixen pàgines com [Debian Contributors][contributors] (on es
pot consultar quina és l'última contribució d'un usuari o equip i a què) i
[Planet][planet], que és un recopilatori de RSS de blogs relacionats amb Debian
i derivats (com Ubuntu), mancava un component amb el que els col·laboradors
puguéssin interactuar.

I aprofitant que actualment hi ha moltíssims dominis de primer nivell (Top-level
Domains, TLD) nous, el `.social` era l'ideal per agrupar tots aquests serveis.

## Som-hi
Només cal anar [debian.social][social]… i veurem que és una pàgina pràcticament
en construcció amb un enllaç a la [Wiki][social-wiki].

I és que és un projecte relativament nou que tot just està incorporant serveis
sota el seu paraigua.

La idea és de muntar serveis que permetin **fer comunitat** al voltant de Debian
evidentment amb programari lliure, i tots ells allotjats en subdominis de
`debian.social`. En els casos on sigui possible i necessari, la identificació es
farà a través del [sistema d'Autenticació única de Salsa][salsa].

## Contacte
Si et preguntes com posar-te en contacte amb l'equip responsable, pots fer-ho
-sí- a través del canal d'IRC `#debian-social`. Però gràcies a la
[integració entre IRC i Matrix][matrix-irc] també ho pots fer amb un
[client de Matrix que suporti autenticació amb SSO com Element][element-web].

## Resultat
Que vols organitzar una xerrada relacionada amb Debian? Doncs pots usar el
sistema de videoconferència [Jitsi][jitsi].

Que prefereixes un sistema de missatgeria instantània? Doncs pots provar la
[instància de Matrix][matrix] i si vols també la
[versió web del client Element][element].

Que tens un vídeo
relacionat amb Debian que vols compartir? Doncs pots usar la instància de
[Peertube][peertube].

Potser et ve de gust un sistema de microblogging? Doncs pots usar la instància
de [Pleroma][pleroma].

Tot i que aquests són només exemples i hi ha més serveis que es preveu que s'hi
afegeixin en un futur, si et ve de gust parlar de Debian i parlar amb la
comunitat de Debian ara tens una ocasió perfecte per a començar.

Recorda que necessitaràs accés a [Salsa][salsa] si hi vols tenir un compte, ja
que pels serveis no federats hauràs de
[crear un tiquet per fer la petició][gitlab-issue].

[contributors]: https://contributors.debian.org/
[planet]: https://planet.debian.org/
[social]: https://debian.social
[social-wiki]: https://wiki.debian.org/Teams/DebianSocial
[salsa]: que-es-salsa.md
[matrix-irc]: com-interactuar-amb-irc-des-de-matrix.md
[element-web]: https://element.io/
[jitsi]: https://jitsi.debian.social/
[matrix]: https://matrix.debian.social/_matrix/static/
[element]: https://element.debian.social/
[peertube]: https://peertube.debian.social/
[pleroma]: https://pleroma.debian.social
[gitlab-issue]: https://salsa.debian.org/debiansocial-team/sysadmin/accounts/-/issues
