# set base image (host OS)
FROM python:3.9-slim

# set the working directory in the container
WORKDIR /code

# install dependencies
RUN pip install mkdocs

# copy the content of the local src directory to the working directory
COPY ./ .

ARG THEME=readthedocs
ARG SITE_DIR=public

RUN mkdir docs && bash -O extglob -c 'mv !(docs|mkdocs.yml) docs/'

RUN mkdocs build --theme $THEME --site-dir $SITE_DIR

# command to run on container start
CMD [ "mkdocs", "serve", "--dev-addr", "0.0.0.0:8080"]
